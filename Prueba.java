import pkg1.*;

public class Prueba
{
    public static void main(String[] args)
    {
        Persona person = new Persona(1222333, "Jose", "Peterson", 35);
        Empleado emp = new Empleado(1222333, "Jose", "Peterson", 35, 448897, 3500000);
        Cliente cli = new Cliente(1222333, "Jose", "Peterson", 35, 59, 200000);
        
        
        person.visualizar();
        emp.visualizar();
        cli.visualizar();
    }
}
