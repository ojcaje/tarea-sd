package pkg1;

public class Cliente extends Persona
{
    protected int idCliente;
    protected int montoCredito;
    
    public Cliente (int NroCedula, String Nombre, String Apellido, int Edad, int idCliente, int montoCredito)
    {
        super(NroCedula, Nombre, Apellido, Edad);
        
        this.idCliente= idCliente;
        this.montoCredito = montoCredito;
    }
    
    
    /* esta clase no puede visualizar los datos especificos del cliente a traves de la funcion heredada visualizar() */
    
}
