package pkg1;

public class Empleado extends Persona
{
    protected int idEmpleado;
    protected int salario;
    
    
    public Empleado (int NroCedula, String Nombre, String Apellido, int Edad, int idEmpleado, int salario)
    {
        super(NroCedula, Nombre, Apellido, Edad);
        this.idEmpleado = idEmpleado;
        this.salario = salario;
    }
    
    @Override
    public void visualizar()
    {
        super.visualizar();
        
        System.out.println("Otros datos del empleado: " + idEmpleado + ", " + salario);
    }
}