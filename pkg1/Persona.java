package pkg1;

public class Persona
{
    protected int NroCedula;
    protected String Nombre;
    protected String Apellido;
    protected int Edad;
    

    public Persona(int NroCedula, String Nombre, String Apellido, int Edad)
    {
        this.Apellido = Apellido;
        this.Edad = Edad;
        this.Nombre = Nombre;
        this.NroCedula = NroCedula;
    }
    
    public void visualizar()
    {
        System.out.println (this.Nombre +" " + this.Apellido + ", " + this.Edad + ", " + this.NroCedula );
    }
}